<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" cdata-section-elements="title"/>
    <xsl:template match="channel/*" priority="0" /> <!-- consume, no output -->
    <xsl:template match="/">
        <xsl:apply-templates select="//item[position() &lt; 5]"/>
    </xsl:template>
    <xsl:template match="item">
        <content-item>
       <p class="news-article__meta"> 
                    <strong>
                        <xsl:value-of select="pubDate" />
                    </strong>
                </p>
           <p class="news-article__title" itemprop="headline">
                    <xsl:element name="a">
                        <xsl:attribute name="href">
                            <xsl:value-of select="link" disable-output-escaping="yes"/>
                        </xsl:attribute>
                        <xsl:attribute name="target">_blank</xsl:attribute>
                        <xsl:value-of select="title" disable-output-escaping="yes"/>
                    </xsl:element>
                </p>
        </content-item>
      </xsl:template>
</xsl:stylesheet>
<!--/xsl:stylesheet>
<xsl:template match="rss">
    <xsl:for-each select="channel/item">
        <div class="news-articles">
            <p class="news-article__meta"> 
                <strong>
                    <xsl:value-of select="/rss/channel/item/pubDate" />
                </strong>
            </p>
            <p class="news-article__title" itemprop="headline">
                <xsl:element name="a">
                    <xsl:attribute name="href">
                        <xsl:value-of select="/rss/channel/item/link" disable-output-escaping="yes"/>
                    </xsl:attribute>
                    <xsl:value-of select="/rss/channel/item/title" disable-output-escaping="yes"/>
                </xsl:element>
            </p>

        </div>
    </xsl:for-each>
</xsl:template>
</xsl:stylesheet-->


           